// Caracterele ce se pot reprezenta in Code39
const digitToCode39 = {
	'0' : '1010001110111010','1' : '1110100010101110',
	'2' : '1011100010101110','3' : '1110111000101010',
	'4' : '1010001110101110','5' : '1110100011101010',
	'6' : '1011100011101010','7' : '1010001011101110',
	'8' : '1110100010111010','9' : '1011100010111010',
	'A' : '1110101000101110','B' : '1011101000101110',
	'C' : '1110111010001010','D' : '1010111000101110',
	'E' : '1110101110001010','F' : '1011101110001010',
	'G' : '1010100011101110','H' : '1110101000111010',
	'I' : '1011101000111010','J' : '1010111000111010',
	'K' : '1110101010001110','L' : '1011101010001110',
	'M' : '1110111010100010','N' : '1010111010001110',
	'O' : '1110101110100010','P' : '1011101110100010',
	'Q' : '1010101110001110','R' : '1110101011100010',
	'S' : '1011101011100010','T' : '1010111011100010',
	'U' : '1110001010101110','V' : '1000111010101110',
	'W' : '1110001110101010','X' : '1000101110101110',
	'Y' : '1110001011101010','Z' : '1000111011101010',
	'-' : '1000101011101110','.' : '1110001010111010',
	' ' : '1000111010111010','*' : '1000101110111010',
	'$' : '1000100010001010','/' : '1000100010100010',
	'+' : '1000101000100010','%' : '1010001000100010'
};

function generateBarcode(text = null) {
    // Adaugam caracterul * (pentru verificare) la inceputul si finalul textului, iar textul il transformam in majuscule.
    var displayText = text.toUpperCase(); //Textul afisat sub codul de bare
    text = '*' + text.toUpperCase() + '*'; // *UPPERCASE TEXT*

    var colors = ''; // Variabila va memora textul convertit in biti.

    var lines = document.getElementById("lines"); // Selectam elementul cu ID #lines. In acest element vom adauga fiecare linie corespunzatoare bit-ului.
    var barcodeText = document.getElementById("barcodeText"); // Selectam locul unde va aparea textul

    lines.innerHTML = ''; // Stergem toate elementele ce au parintele elementul cu ID #lines
    barcodeText.innerHTML = ''; // Stergem toate elementele ce au parintele elementul cu ID #barcodeText

    for (let i = 0; i < text.length; i++) {
        colors = colors + digitToCode39[text[i]]; // Adaugam in variabila colors reprezentarea liniei (negru sau alb) aferenta fiecarui caracter din variabila text.
    }

    for(let i = 0; i < colors.length; i++) {
        // Se creaza liniile pentru textul introdus
        var line = document.createElement('div');
        line.className = 'line-' + colors[i];
        lines.appendChild(line);
    }

    barcodeText.innerHTML = displayText;
}